#include <Arduino.h>
#include <Wire.h>
#include "Adafruit_SHT31.h"
#include <SPI.h>
#include <SD.h>
#include "RTClib.h"
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/power.h>

//#define DEBUG 1

const int chipSelect = 10;
File myFile;

Adafruit_SHT31 sht31 = Adafruit_SHT31();

RTC_DS1307 rtc;

const int powerPin = 3; // SD
const int powerPin2 = 8; // RTC
const byte LED = 9;

const long InternalReferenceVoltage = 1062;  // Adjust this value to your board's specific internal BG voltage

int turn = 0;

// watchdog interrupt
ISR (WDT_vect) 
{
   wdt_disable();  // disable watchdog
}  // end of WDT_vect

void watchdogEnable() {
      // disable ADC
    ADCSRA = 0;  

    // clear various "reset" flags
    MCUSR = 0;     
    // allow changes, disable reset
    WDTCSR = bit (WDCE) | bit (WDE);
    // set interrupt mode and an interval 
    WDTCSR = bit (WDIE) | bit (WDP3) | bit (WDP0);    // set WDIE, and 8 seconds delay
    wdt_reset();  // pat the dog
  
    set_sleep_mode (SLEEP_MODE_PWR_DOWN);  
    sleep_enable();

    noInterrupts ();           // timed sequence follows

    // will be called when pin D2 goes low  
    //attachInterrupt (0, wakeUp, FALLING);
    //attachInterrupt (1, wakeUpConf, FALLING);
    
    EIFR = bit (INTF0);  // clear flag for interrupt 0
    EIFR = bit (INTF1);  // clear flag for interrupt 1
    
    // turn off brown-out enable in software
    MCUCR = bit (BODS) | bit (BODSE);
    MCUCR = bit (BODS); 
    interrupts ();             // guarantees next instruction executed
    sleep_cpu ();  
  
    // cancel sleep as a precaution
    sleep_disable();    

}


int getBandgap () 
  {
  // REFS0 : Selects AVcc external reference
  // MUX3 MUX2 MUX1 : Selects 1.1V (VBG)  
   ADMUX = bit (REFS0) | bit (MUX3) | bit (MUX2) | bit (MUX1);
   ADCSRA |= bit( ADSC );  // start conversion
   while (ADCSRA & bit (ADSC))
     { }  // wait for conversion to complete
   int results = (((InternalReferenceVoltage * 1024) / ADC) + 5) / 10; 
   return results;
  } // end of getBandgap

void flashBattery ()
  {
  pinMode (LED, OUTPUT);
  for (byte i = 0; i < 10; i++)
    {
    digitalWrite (LED, HIGH);
    delay (50);
    digitalWrite (LED, LOW);
    delay (50);
    }
  pinMode (LED, INPUT);
    
  }  // end of flash
  
void setup() {
  #ifdef DEBUG
  Serial.begin(9600);
  while (!Serial)
    delay(10); 
  Serial.println("Starting"); 
  #endif
  
  pinMode(powerPin, OUTPUT);
  digitalWrite(powerPin, HIGH);
  SD.begin();
  delay(10);
  
  pinMode(powerPin2, OUTPUT);
  digitalWrite(powerPin2, HIGH);  
  //Wire.begin();

  //sht31.begin(0x44);
  rtc.begin();
  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running, let's set the time!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  
  #ifdef DEBUG
  DateTime now = rtc.now();
  Serial.print(now.year(), DEC);
  Serial.print('/');
  Serial.print(now.month(), DEC);
  Serial.print('/');
  Serial.print(now.day(), DEC);
  Serial.print(" ");
  Serial.print(now.hour(), DEC);
  Serial.print(':');
  Serial.print(now.minute(), DEC);
  Serial.print(':');
  Serial.print(now.second(), DEC);
  Serial.println();
  Serial.flush();
  #endif

  // cut power to external devices
  pinMode(powerPin, INPUT);
  digitalWrite(powerPin, LOW);
  delay(10);
  
  pinMode(powerPin2, INPUT);
  digitalWrite(powerPin2, LOW);  
  
  // turn off I2C
  TWCR &= ~(bit(TWEN) | bit(TWIE) | bit(TWEA));
 
  // turn off I2C pull-ups
  digitalWrite (A4, LOW);
  digitalWrite (A5, LOW);
 
}

void loop() {
  if (turn >= 37) { // wait 5 minutes
    // power external devices
    pinMode(powerPin, OUTPUT);
    digitalWrite(powerPin, HIGH);  
    //power_spi_enable();
    delay(10);
    SD.begin();

    pinMode(powerPin2, OUTPUT); 
    digitalWrite(powerPin2, HIGH);
  
    Wire.begin();
    rtc.begin();
    sht31.begin(0x44);
    float t = sht31.readTemperature();
    float h = sht31.readHumidity();
    DateTime now = rtc.now();
    if ((! isnan(t)) & (! isnan(h))) {
      int y = now.year();
      int m = now.month();
      int d = now.day();
      int H = now.hour();
      int M = now.minute();
      int S = now.second();
    
      #ifdef DEBUG
      Serial.print(y, DEC);
      Serial.print('/');
      Serial.print(m, DEC);
      Serial.print('/');
      Serial.print(d, DEC);
      Serial.print(" ");
      Serial.print(H, DEC);
      Serial.print(':');
      Serial.print(M, DEC);
      Serial.print(':');
      Serial.print(S, DEC);
      Serial.print("\t");
      Serial.print("*C:"); 
      Serial.print(t); Serial.print(",");
      Serial.print("RH%:"); 
      Serial.print(h);
      Serial.println();
      Serial.flush();
      #endif
    
      myFile = SD.open("data.log", FILE_WRITE);
      myFile.print(y,DEC);
      myFile.print("-");
      myFile.print(m,DEC);
      myFile.print("-");
      myFile.print(d,DEC);
      myFile.print(" ");
      myFile.print(H, DEC);
      myFile.print(':');
      myFile.print(M, DEC);
      myFile.print(':');
      myFile.print(S, DEC);
      myFile.print("\t");
      myFile.print(t);
      myFile.print("\t");
      myFile.println(h);
      myFile.close();
    }

    // cut power to external devices
    pinMode(powerPin, INPUT);
    digitalWrite(powerPin, LOW);
    delay(10);
  
    pinMode(powerPin2, INPUT);
    digitalWrite(powerPin2, LOW);  
  
    // turn off I2C
    TWCR &= ~(bit(TWEN) | bit(TWIE) | bit(TWEA));
 
    // turn off I2C pull-ups
    digitalWrite (A4, LOW);
    digitalWrite (A5, LOW);
    turn = 0; 
  } 
  turn++; 
  getBandgap(); 
  if (getBandgap() < 310) { // If voltage below 3 volts blink led
    flashBattery();
  }
  watchdogEnable();
}
